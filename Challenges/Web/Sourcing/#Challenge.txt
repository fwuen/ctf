Name:
    Sourcing
Description:
    People tend to make simple things too complicated and complicated things too easy.
Files:
    index.html
    signin.css
    signin.js
Hints:  
    Check the comments! | 20 Points
    <Another Hint | Amount of Points>
Category:
    Web
Points/Value:
    30
Flag:
    THICTF{$сНr0Ed!ngers_с@T_much_grumpy}