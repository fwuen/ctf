Name:
    strings reloaded
Description:
    Aaaah, again a lot of strings. The flag format is THICTF{<clear-text>} where <clear-text> is the decrypted string of the displayed hash.
    Find the correct flag in the binary!    
    Hint: You will not be able to brute force the hash.
    Find the executable on port 2225 as user ctf19 with password ctf19.
Files:
    strings-reloaded (binary)
Hints:  
    /
Category:
    Reverse Engineering
Points/Value:
    240
Flag:
    THICTF{mDezcSKFsQiAmbhWOXczvxpT}
